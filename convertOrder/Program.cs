﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;
using FirebirdSql.Data.FirebirdClient;

namespace ConvertOrder
{
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon = @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 1
//ID заказа    : 
//Клиентский ID: [ZakazCode]
//Дата заказа  : [DataOrder]
//Позиций      : [Rows]
//Версия EXE   : 
//Версия CFG   : 
//Статус CFG   : 
//Прайс-лист   : 
//Комментарий  : [Comment]
//";
           
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                string prefix = String.Empty;
                #region Проверка параметров
                if (args.Length < 2)
                {
                    System.Console.WriteLine("Convertpanatec <файл заявки> <файл шаблона> <префикс кода адреса доставки>");
                    return 1;
                }

                if (!System.IO.File.Exists(args[0]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Не найден файл заявки");
                    return 1;
                }
                if (!System.IO.File.Exists(args[1]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Не найден файл шаблона");
                    return 1;
                }   
                if (args.Length > 2 )
                {
                    prefix = args[2];
                }   
                #endregion
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + args[0]);
                //Читаем файл заявки
                List<Order> Orders = new List<Order>();
                DataTable excel = ReadExcelToTable(args[0]);
                List<Row> rowsPanatec = null;
                
                int i=0;
                string codedostavki = "";
                int counter=1;
                Cod cod = new Cod();
                int count = 0;
                string ClientCodeAddress= String.Empty;
                Order orderPanatec=null;
               
                rowsPanatec = new List<Row>();

                // Новый цикл для создания заявок
                foreach (DataRow row in excel.Rows)
                {
                   ClientCodeAddress = row[1].ToString().Trim();
                   if (String.IsNullOrEmpty(ClientCodeAddress)) continue;
                   if (Orders.Find(c => c.ClientCodeAddress == ClientCodeAddress) == null)
                   {
                       orderPanatec = new Order();
                       orderPanatec.DataOrder = DateTime.Today.ToString("dd.MM.yyyy");
                       orderPanatec.CodeZakaz = "Торги";
                       orderPanatec.Comment = row[0].ToString().Trim() + " " + row[2].ToString().Trim();
                       if (!string.IsNullOrEmpty(row[1].ToString().Trim())) cod = GetCodeFromASU(prefix + ClientCodeAddress);
                       orderPanatec.CodeDostavki = cod.CodeDostavki;
                       orderPanatec.CodeClient = cod.CodeClient;
                       orderPanatec.ClientCodeAddress = ClientCodeAddress;
                       orderPanatec.rows = new List<Row>();
                       Orders.Add(orderPanatec);
                   }
                    Row roworder = new Row();
                    if (row[6].ToString().Length > 0 && row[9].ToString().Length > 0)
                    { roworder.Code = row[9].ToString(); roworder.Count = row[6].ToString(); }
                    Order buffer = Orders.Find(c => c.ClientCodeAddress == ClientCodeAddress);
                    buffer.rows.Add(roworder);
                }




                //foreach (DataRow row in  excel.Rows)
                //{
                   
                //    if (counter == 1)
                //    {
                        
                //        orderPanatec.DataOrder = DateTime.Today.ToString("dd.MM.yyyy");
                //        orderPanatec.CodeZakaz = "Торги";
                //        orderPanatec.Comment = row[0].ToString().Trim() + " " + row[2].ToString().Trim();
                //        ClientCodeAddress = row[1].ToString().Trim();
                //        if (!string.IsNullOrEmpty(row[1].ToString().Trim())) cod = GetCodeFromASU(prefix + ClientCodeAddress );
                //        orderPanatec.CodeDostavki = cod.CodeDostavki;
                //        orderPanatec.CodeClient = cod.CodeClient;
                //        counter = 2;
                //    }
                //    if (counter == 2)
                //    {
                //        if (ClientCodeAddress.Equals(row[1].ToString().Trim()))
                //        {
                //            if (row[6].ToString().Length > 0 && row[9].ToString().Length > 0)
                //                rowsPanatec.Add(new Row() { Code = row[9].ToString(), Count = row[6].ToString() });
                //        }
                //        else
                //        {
                //            orderPanatec.rows = rowsPanatec;
                //            orderPanatec.Rows = rowsPanatec.Count.ToString();
                //            orderPanatec.ClientCodeAddress = ClientCodeAddress;
                //            Orders.Add(orderPanatec);
                //            orderPanatec = new Order();
                //            rowsPanatec = new List<Row>();
                //            if (row[6].ToString().Length > 0 && row[9].ToString().Length > 0)
                //                rowsPanatec.Add(new Row() { Code = row[9].ToString(), Count = row[6].ToString() });
                //            counter = 1;
                //        }
                //        count++;
                //        if (count == excel.Rows.Count)
                //        {
                //            orderPanatec.rows = rowsPanatec;
                //            orderPanatec.Rows = rowsPanatec.Count.ToString();
                //            if (orderPanatec.rows.Count>0) Orders.Add(orderPanatec);
                //        }

                //    }

                   
                //}
                string[] shablon = File.ReadAllLines(args[1], encoding);
                int countorder = 1;
                foreach (var item in Orders)
                {
                    //формируем заявку СИА
                    StringBuilder ordersia = new StringBuilder();
                    ordersia.Append(string.Join("\r\n", shablon).Replace("[CodeZakaz]", item.CodeZakaz)
                                                                        .Replace("[CodeZakaz]", item.CodeZakaz)
                                                                        .Replace("[CodeClient]", item.CodeClient)
                                                                        .Replace("[CodeDostavki]", item.CodeDostavki)
                                                                        .Replace("[DataOrder]", item.DataOrder)
                                                                        .Replace("[Rows]", item.Rows)
                                                                        .Replace("[Comment]", item.Comment)).AppendLine();

                    //пишем в файл 

                    foreach (var row in item.rows)
                    {
                        ordersia.Append(new string(' ', 50 - row.Code.Length)).Append(row.Code).Append(new string(' ', 10 - row.Count.Length)).Append(row.Count).AppendLine();
                    }
                    ordersia.AppendLine();
                    //пишем в файл 
                    File.WriteAllText(args[0] + "_" + item.ClientCodeAddress.ToString() + "_" + countorder.ToString() + ".txt", ordersia.ToString(), encoding);
                    countorder++;
                    //File.WriteAllText(Path.Combine(Path.GetDirectoryName(args[0]), "P" + Path.GetFileName(args[0]).Replace(".xls", ".txt")), ordersia.ToString(), encoding);
                }
               
              
            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }



            return 0;
        }
        public class Order
        {
            public string CodeClient { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string CodeZakaz { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string Comment { get; set; }
            public string ClientCodeAddress { get; set; }
            public List<Row> rows { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
        }
        public static  DataTable ReadExcelToTable(string path)
        {

            //Connection String

            //string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";
            //the same name 
            //string connstring = Provider=Microsoft.JET.OLEDB.4.0;Data Source=" + path + //";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';"; 

            //using (OleDbConnection conn = new OleDbConnection(connstring))
            //{
                //conn.Open();
                ////Get All Sheets Name
                //DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });

                ////Get the First Sheet Name
                //string firstSheetName = sheetsName.Rows[0][2].ToString();

                ////Query String 
                //string sql = string.Format("SELECT * FROM [{0}]", firstSheetName);
                //OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
                //DataSet set = new DataSet();
                //ada.Fill(set);
                //return set.Tables[0];            
                    //System.Data.OleDb.OleDbConnection MyConnection;
        //}
     //       string connectionString =
    //"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
    //file_name +
    //";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                     //using (System.Data.OleDb.OleDbConnection connExcel = new System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + path + "';Extended Properties=Excel 8.0;"))
            using (System.Data.OleDb.OleDbConnection connExcel = new System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + path + "';Extended Properties=\"Excel 12.0 Xml;HDR=YES\""))
                     {
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "TABLE" });
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        DataSet ds = new DataSet();
                        System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select  * from [" + SheetName + "]", connExcel);
                        MyCommand.Fill(ds);
                        return ds.Tables[0];
                    }
            
        }
        private static Cod GetCodeFromASU(string addresscode)
        {
            Cod cod = new Cod();
            using (FbConnection RefConnection = new FbConnection("User ID=TWUSER;Password=54321; Database=./db/Siafil.gdb; DataSource=asusrv; Charset=NONE; providerName=\"System.Data.SqlClient\""))
            {
                try
                {

                    RefConnection.Open();
                    FbCommand readCommand = new FbCommand(String.Format("select first 1 nid,nidlib from adrdost a join nab n on a.adrdostid = n.nid where a.adrdostcode='{0}'", addresscode), RefConnection);
                    FbDataReader myreader = readCommand.ExecuteReader();
                    while (myreader.Read())
                    {
                        cod.CodeDostavki = myreader.GetString(0);
                        cod.CodeClient = myreader.GetString(1);
                    }
                    RefConnection.Close();
                }
                catch
                {
                    throw new Exception("Ошибка при подключении к базе данных");
                }
            }
            return cod;
        }
        public class Cod
        {
            public string CodeDostavki { get; set; }
            public string CodeClient { get; set; }
        }
    }
}
